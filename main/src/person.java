package Main;

import java.sql.Date;
import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TextField;
import java.io.*;

public class person {
	
	private SimpleStringProperty name ;
	private SimpleStringProperty surname;
	private SimpleStringProperty  adress;
	private SimpleStringProperty  nip;
	private SimpleStringProperty  pesel;
	private SimpleStringProperty  date;
	//boolean nip_is_good = new Boolean(true);
	//boolean pesel_is_good = new Boolean(true);
	
	
	
	public person(String name, String surname, String adress1, String adress2, String nip, String pesel, String date) {
		super();
		this.name = new SimpleStringProperty(name);
		this.surname = new SimpleStringProperty(surname);
		this.adress = new SimpleStringProperty(adress1 + " " + adress2);
		this.nip = new SimpleStringProperty(nip);
		this.pesel = new SimpleStringProperty(pesel);
		this.date = new SimpleStringProperty(date);
	}
	
	public person() {
		
	}
	
	public static void clean_FieldText(TextField a) {
		a.setText("");
	}

	public String getName() {
		return name.get();
	}
	public String getSurname() {
		return surname.get();
	}
	
	public String getAdress() {
		return adress.get();
	}

	public String getNip() {
		return nip.get();
	}

	public String getPesel() {
		return pesel.get();
	}
	
	public String getDate() {
		return date.get();
	}
	
	
	public static boolean check_pesel(String pesel_to_check) {
		boolean check=true;
		int month;
		int day;
		pesel_to_check = pesel_to_check.replaceAll(" ", "");
		if(pesel_to_check.length() == 11 )
		{
			for(int i=0; i<pesel_to_check.length(); i++ )
			{
				for(int b=1;b<10;b++) {
					if(pesel_to_check.charAt(i) == (char)b) check=false;
				}
				if (check==false)
					return false;
			}
			month = 10 *Character.getNumericValue(pesel_to_check.charAt(2)) + Character.getNumericValue(pesel_to_check.charAt(3));
			day = 10*Character.getNumericValue(pesel_to_check.charAt(4)) + Character.getNumericValue(pesel_to_check.charAt(5));
			if((day >0 && day < 32) &&(month == 1 || month == 3 || month == 5 ||month == 7 || month == 8 || month == 10 ||month == 12)) {
					return true; 
			} else if((day >0 && day < 31) &&(month == 2 || month == 4 || month == 6 ||month == 11 || month == 9)) {
				return true; 
			}
			return false;	
		}
		return false;
	}
	
	
	public static boolean checkNip(String nipToCheck) {
		boolean check = true;
		if(nipToCheck.charAt(0) == 'P'){
			nipToCheck.substring(0, 2);
		}
		nipToCheck = nipToCheck.replaceAll("-", "");
		if (nipToCheck.length() != 10) {
			return false;
		}
		for(int i=0; i<nipToCheck.length(); i++ )
		{
			for(int b=1;b<10;b++) {
				if(nipToCheck.charAt(i) == (char)b) check=false;
			}
			if (check==false)
				return false;
		}
		int[] weights = {6, 5, 7, 2, 3, 4, 5, 6, 7};
		int sum=0;
		for(int i=0;i<9; i++) {
			sum+=weights[i]*Character.getNumericValue(nipToCheck.charAt(i)) ;
		}
			return (sum % 11) == Integer.parseInt(nipToCheck.substring(9, 10));	
	}


	@Override
	public String toString() {
		return "imie "+ name + " surname=  " + surname  +"adress= " + adress + "NIP= " + nip + "Pesel= " + pesel;
	}


}
