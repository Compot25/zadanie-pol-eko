package Main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import Main.person;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class mainController implements Initializable{

	Stage stage;
	ObservableList<person> osobyList = FXCollections.observableArrayList();
	
	public Stage getStage() {
		return stage;
	}
	
	public void setStage(Stage primaryStage) {
		this.stage = primaryStage;
	}
	
	@FXML
	Label Name_FieldText_Variable, Attention_Communicate;
	
	@FXML
	Button Clean_Button ,AddBase_Button, DataBase_Button;
	
	@FXML
	TextField Name_TextField, Surname_TextField, Adress2_TextField, Adress1_TextField, Nip_TextField, Pesel_TextField ;
	
	@FXML
	DatePicker Date_TextField;
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {					//czysczenie wszytkicf TextField
		click_Button_Clean();
	}
	
	//--------------------------box------------------------
	public void closeApplication() {													//zamykanie aplikacji
		Platform.exit();
		System.exit(0);
	}
	
	
	public void setCaspian() {															//ustawianie stylu Caspian
		Application.setUserAgentStylesheet("CASPIAN");
	}
	

	public void setModera() {															//ustawianie stylu Modera
		Application.setUserAgentStylesheet("MODENA");
	}
	
	
	public void aboutApplication() {													//wyświetlanie inforamcji o aplikacji
	
	}
	//--------------------------------------------------------
	
	//---------------------------Button--------------------
	public void click_Button_Clean() {
		person.clean_FieldText(Name_TextField);
		person.clean_FieldText(Surname_TextField);
		person.clean_FieldText(Adress2_TextField);
		person.clean_FieldText(Adress1_TextField);
		person.clean_FieldText(Nip_TextField);
		person.clean_FieldText(Pesel_TextField);
		Attention_Communicate.setText("");
		Date_TextField.setValue(null);
	}
	
	
	public void click_Button_Base() throws IOException {								//obsluga przycisku dane
		//click_Button_Add();
		//click_Button_Clean();
		Stage stage = new Stage();
		FXMLLoader fxmLoader = new FXMLLoader();										
		StackPane stackPane = fxmLoader.load(getClass().getResource("data.fxml").openStream());
		dataController controller = (dataController) fxmLoader.getController();

		controller.getBase(osobyList);													//przeysyla dane do okna2 z danymi
		
		Scene scene = new Scene(stackPane);											
		stage = new Stage();
		stage.setTitle("Dane");
		stage.setResizable(false);
		stage.setScene(scene);
		stage.getIcons().add(new Image("Main/PolEkoLogo.png"));							//dodanie logo
		stage.initModality(Modality.APPLICATION_MODAL);									//zablokowanie pozostalych okien
		stage.show();
	}
	
	
	public void click_Button_Add() throws IOException{									//obsluga przycisku dodaj
		
		if((!(Name_TextField.getText().equals(""))) && (!(Surname_TextField.getText().equals(""))) && (!(Adress1_TextField.getText().equals(""))) && (!(Adress2_TextField.getText().equals(""))) && (!(Nip_TextField.getText().equals(""))) && (!(Pesel_TextField.getText().equals(""))) ){
			osobyList.add(new person(Name_TextField.getText() , Surname_TextField.getText(), Adress1_TextField.getText(), Adress2_TextField.getText(), Nip_TextField.getText(), Pesel_TextField.getText(), Date_TextField.getValue().toString()));
			click_Button_Clean();
		}else {	
		Attention_Communicate.setText("Uzupełnij wszytkie pola");
		}

	}
}
