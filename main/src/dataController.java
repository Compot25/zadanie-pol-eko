package Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import Main.person;
import java.util.ArrayList;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.text.TabableView;

public class dataController implements Initializable{

	static ObservableList<person> table_person = FXCollections.observableArrayList();

	@FXML
	Label wrongNIP, wrongPESEL;
	
	@FXML private TableView<person> tableView1;
	@FXML private TableColumn<person, String> name;
	@FXML private TableColumn<person, String> surname;
	@FXML private TableColumn<person, String> adress;
	@FXML private TableColumn<person, String> nip;
	@FXML private TableColumn<person, String> pesel;
	@FXML private TableColumn<person, String> date;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {						
		name.setCellValueFactory(new PropertyValueFactory<person, String>("name"));
		surname.setCellValueFactory(new PropertyValueFactory<person, String>("surname"));
		adress.setCellValueFactory(new PropertyValueFactory<person, String>("adress"));
		nip.setCellValueFactory(new PropertyValueFactory<person, String>("nip"));
		pesel.setCellValueFactory(new PropertyValueFactory<person, String>("pesel"));
		date.setCellValueFactory(new PropertyValueFactory<person, String>("date"));
		tableView1.setItems(table_person);
	}
	
	
	
	public void getBase(ObservableList<person> osobyList) {
		wrongNIP.setText("");							//czyszczenie Label do wyswietlania niepoprawnego nipu
		wrongPESEL.setText("");							//czyszczenie Label do wyswietlania niepoprawnego peselu
		String fillNip = new String("");				//przechwojue niepoprawne nipy
		String fillPesel = new String("");				//przechowuje niepoprawne pesele
		table_person.clear();							
		table_person.addAll(osobyList);
		for(person a: table_person)						//kontrolenie wyświetlam odebrane dane
		{System.out.println(a.toString());}
		for(person c: table_person)
		{
			System.out.println(c.getNip());
			if(!(person.checkNip(c.getNip()))) {
				fillNip += c.getNip() + ", " ;
			}
			if(!(person.check_pesel(c.getPesel()))){
				fillPesel += c.getPesel() +", ";
			}
		}
		wrongNIP.setText(fillNip);
		wrongPESEL.setText(fillPesel);
		
	}

}
